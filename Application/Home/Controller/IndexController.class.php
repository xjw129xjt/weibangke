<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;

use OT\DataDictionary;

/**
 * 前台首页控制器
 * 主要获取首页聚合数据
 */
class IndexController extends HomeController
{

    //系统首页
    public function index()
    {

        $new = getSectionName('最新动态');
        $config_sectiion = getSectionName(C('DEFAULT_RIGHT_NAME'));

        $this->assign('new', $new);
        $this->assign('config_sectiion', $config_sectiion);

        $ann = D('announcement')->where('id=1')->find();
        $this->assign('ann', $ann); //最新公告


        //redirect(U('Weibo/Index/index'));
        $category = D('Category')->getTree();
        $lists = D('Document')->lists(null);
        $this->assign('category', $category); //栏目
        $this->assign('lists', $lists); //列表
        $this->assign('page', D('Document')->page); //分页
        $this->display();
    }

    public function products($id = null)
    {

        $category = D('Category')->where(array('title' => '产品服务套餐'))->find();
        $list = D('Document')->lists($category['id'], '(case level when 0 then 1 else 0 end),level');
        $this->assign('artical_list', $list);
        $id == null && $id = $list[0]['id'];
        $artical = D('Document')->detail($id);
        $this->assign('artical_id', $id);
        $this->assign('artical', $artical);
        $this->display();
    }


    public function cases($id = null)
    {
        $category = D('Category')->where(array('title' => '经典案例'))->find();
        $category = D('Category')->getTree($category['id']);
        foreach ($category['_'] as $k => &$v) {

        }

        $this->assign('category', $category);

        $id == null && $id = $category['_'][0]['id'];
        $list = D('Document')->lists($id, '(case level when 0 then 1 else 0 end),level');
        foreach ($list as $key => $val) {
            $artical = D('Document')->detail($val['id']);
            $artical_list[$key] = $artical;
            $artical_list[$key]['picture_url'] = getRootUrl() . D('picture')->where('id=' .$val['cover_id'] )->getField('path');
        }
        $this->assign('artical_list', $artical_list);
        $this->assign('cate_id', $id);
        //dump($artical_list);
        $this->display();
    }
}