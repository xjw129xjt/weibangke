<?php
/**
 * Created by PhpStorm.
 * User: caipeichao
 * Date: 14-3-11
 * Time: PM5:41
 */

namespace Admin\Controller;

use Admin\Builder\AdminListBuilder;
use Admin\Builder\AdminConfigBuilder;
use Admin\Builder\AdminSortBuilder;

class DefaultController extends AdminController
{


    public function index() {
        $post = M('Announcement')->where('id=1')->find();
        //显示页面
        $builder = new AdminConfigBuilder();
        $builder->title( '编辑公告')
            ->keyId()
            ->keyTitle()
            ->keyEditor('content','内容')
            ->keyCreateTime()
            ->buttonSubmit(U('doEditAnnouncement'))->buttonBack()
            ->data($post)
            ->display();
    }
    public function doEditAnnouncement($id=1,$title,$content,$create_time){

        $data['title'] = $title;
        $data['content'] = $content;
        $data['ctime'] = $create_time;
        $res = D('Announcement')->where('id=1')->save($data);
        if(!$res){
           $result = D('Announcement')->add($data);
        }
        if($res || $result ){
            $this->success( '编辑成功');
        }else{
            $this->success( '编辑失败');
        }
    }

    public function other($id=null) {

        $post['right_name'] =  C('DEFAULT_RIGHT_NAME');
        //显示页面
        $builder = new AdminConfigBuilder();
        $builder->title( '其他配置')
            ->keyText('right_name','右侧论坛名配置项')
            ->buttonSubmit(U('doEditRightName'))->buttonBack()
            ->data($post)
            ->display();
    }

    public function doEditRightName($right_name){

        $data['name'] = 'DEFAULT_RIGHT_NAME';
        $data['type'] = 1;
        $data['title'] = '网站首页右侧论坛版块';
        $data['group'] = 0;
        $data['update_time'] = time();
        $data['status'] = 1;
        $data['value'] = $right_name;
        $res = D('Config')->where(array('name'=>'DEFAULT_RIGHT_NAME'))->save($data);
        S('DB_CONFIG_DATA',null);
        if(!$res){
            $data['create_time']=time();
            $result = D('Config')->add($data);
        }
        if($res || $result ){
            $this->success( '编辑成功');
        }else{
            $this->success( '编辑失败');
        }
    }


}
